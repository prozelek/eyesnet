function [ err1, err2, err1Indexes, err2Indexes ] = calcErrs(result,groundTruth)
%return type I error "err1" with example's indexes "err1Indexes"
%and type II error "err2" with example's indexes "err2Indexes"
    err1=0;
    err2=0;
    k1=0;
    k2=0;
    for testI=1:length(groundTruth)
        if (groundTruth(testI)==1)&&(result(testI)~=1)
            err1=err1+1;
            k1=k1+1;
            err1Indexes(k1)=testI;
        end       
        if (groundTruth(testI)==0)&&(result(testI)~=0)
            err2=err2+1;
            k2=k2+1;
            err2Indexes(k2)=testI;
        end        
    end
    lenPos=sum(groundTruth==1); 
    err1=err1/lenPos;
    lenNeg=sum(groundTruth==0);
    err2=err2/lenNeg; 
end

