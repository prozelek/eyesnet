function [accuracy, targets, outputs] = TestConvNet(net, testImages, testLabels)

YTest = classify(net,testImages);
numClasses = length(unique(testLabels));
accuracy = sum(YTest == testLabels)/numel(testLabels);
outputs = zeros(numClasses,length(testLabels));
for i=1:length(YTest);
    outputs(uint8(YTest(i)),i) = 1;
    targets(uint8(testLabels(i)),i) = 1;
end;
plotconfusion(targets,outputs);