function [images, imagesPath] = loadDir(path)
%load all images from path into 3d matrix images (first index is image
%number) and save images path into imagesPath
    dirPos=dir(path);
    len=length(dirPos);
    k=0;
    images=zeros(len-2,24,24,'uint8');
    for i=3:len
        k=k+1;
        pathIm=cat(2,path,dirPos(i).name);
        images(k,:,:)=imread(pathIm);
        imagesPath{k}=pathIm;
    end
end

