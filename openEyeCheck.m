function is_open = openEyeCheck(inpIm, brain)
%predict image with path "inpIm" by dint of classifier "brain"
%example:
%   load brain.mat
%   is_open=openEyeCheck(image_path);
    inpImMat=double(imread(inpIm));
    load brain.mat;   
    m=mean(inpImMat(:));
    sko=std(inpImMat(:));
    imPreproc(:,:,1,1) = (inpImMat-m)./sko;
    res=classify(brain,imPreproc);
    is_open=grp2idx(res)-1;
end