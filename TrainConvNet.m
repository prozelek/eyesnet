function net = TrainConvNet(TrainData, TrainLabels)

numClasses=2;
%LAYOUT FOR 6.6%
% l(1) = imageInputLayer([24, 24, 1], 'Normalization', 'none','DataAugmentation',{'randfliplr'});
% l(end+1) = convolution2dLayer(5,20);
% l(end+1) = maxPooling2dLayer(2,'Stride',2);
% l(end+1) = fullyConnectedLayer(7);
% l(end+1) = fullyConnectedLayer(numClasses);
% l(end+1) = softmaxLayer();
% l(end+1) = classificationLayer();


l(1) = imageInputLayer([24, 24, 1], 'Normalization', 'none','DataAugmentation',{'randfliplr'});
l(end+1) = convolution2dLayer(3,60);
l(end+1) = dropoutLayer();
l(end+1) = maxPooling2dLayer(2,'Stride',2);
l(end+1) = fullyConnectedLayer(7);
l(end+1) = fullyConnectedLayer(numClasses);
l(end+1) = softmaxLayer();
l(end+1) = classificationLayer();

opts = trainingOptions('sgdm','MaxEpochs',100);

net = trainNetwork(TrainData,TrainLabels,l,opts);