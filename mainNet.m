function [Data] = mainNet()
% [imPos, imPosPath]=loadDir('SplitTrain/pos/');
% [imNeg, imNegPath]=loadDir('SplitTrain/neg/');
% lenPos=size(imPos,1);
% lenNeg=size(imNeg,1);
% data=cat(1,imPos,imNeg);
% groundTruth=cat(1,ones(lenPos,1),zeros(lenNeg,1));
%  save data.mat;
load('data.mat');
%%%%%%%%%%PARAMETERS%%%%%%%%%%%%
sh=0;
del = 2/3;
doPreNorm=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[baseTrain, baseTest,groundTruthTrain,groundTruthTest] = splitBase(imNeg,imPos,del);
[imagesTrain] = extractData(baseTrain,sh, doPreNorm);
[imagesTest] = extractData(baseTest,sh, doPreNorm);
imagesTrain=cell2mat(reshape(imagesTrain,1,1,1,length(imagesTrain)));
imagesTest=cell2mat(reshape(imagesTest,1,1,1,length(imagesTest)));
groundTruthTrain=categorical(groundTruthTrain);
groundTruthTest=categorical(groundTruthTest);

brain = TrainConvNet (imagesTrain, groundTruthTrain');
[accuracy, targets, outputs] = TestConvNet(brain, imagesTest, groundTruthTest');

% Data.TrainImages = baseTrain;
% Data.TestImages = baseTest;
% Data.net = brain;
% Data.accuracy = accuracy;
% Data.classAccuracy.outputs = outputs;
% Data.classAccuracy.targets = targets;

save('brain1.mat','brain');

dataPath=cat(2,imPosPath,imNegPath);
for testIm=1:length(groundTruth)
    result(testIm)=openEyeCheck(dataPath{testIm});    
end
[err1, err2] = calcErrs(result,groundTruth)
end

function [baseTrain, baseTest,groundTruthTrain,groundTruthTest] = splitBase(imNeg,imPos,del)
rpN = randperm(length(imNeg));
rpP = randperm(length(imPos));
stopIndN = round(length(rpN)*del);
stopIndP = round(length(rpP)*del);
baseTrain = cat(1,imNeg(rpN(1:stopIndN),:,:), imPos(rpP(1:stopIndP),:,:));
groundTruthTrain = [zeros(1,stopIndN) ones(1,stopIndP)];
baseTest = cat(1,imNeg(rpN(stopIndN+1:end),:,:), imPos(rpP(stopIndP+1:end),:,:));
groundTruthTest = [zeros(1,length(imNeg)-stopIndN) ones(1,length(imPos)-stopIndP)];
rpT = randperm(length(baseTrain));
baseTrain = baseTrain(rpT,:,:);
groundTruthTrain=groundTruthTrain(rpT);
end

function [images] = extractData(base,sh, doPreNorm)
for i=1:size(base,1);
    img=double(squeeze(base(i,:,:)));
    if (doPreNorm)
        ichan = img;
        m=mean(ichan(:));
        sko=std(ichan(:));
        img(:,:) = (ichan-m)./sko;
    end;
    if (sh>0)
        imgOld=img;
        img=zeros(size(imgOld,1)+2*sh, size(imgOld,2)+2*sh, size(imgOld,3));
        img(sh+1:end-sh, sh+1:end-sh, :)=imgOld;
    end;
    images{i} = img;
end;
end
